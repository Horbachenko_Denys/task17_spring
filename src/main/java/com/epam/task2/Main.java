package com.epam.task2;

import com.epam.task2.bean.beans1.BeanA;
import com.epam.task2.bean.beans1.BeanB;
import com.epam.task2.bean.beans1.BeanC;
import com.epam.task2.bean.beans2.NarcissusFlower;
import com.epam.task2.bean.beans2.RoseFlower;
import com.epam.task2.bean.beans3.BeanD;
import com.epam.task2.bean.flower.FlowerOrder;
import com.epam.task2.bean.other.OtherBeanB;
import com.epam.task2.bean.other.OtherBeanC;
import com.epam.task2.config.MyConfigOne;
import com.epam.task2.config.MyConfigTwo;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.core.env.AbstractEnvironment;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.getEnvironment().setActiveProfiles("test");
        context.register(MyConfigOne.class);
        context.refresh();

        System.out.println("===== Config One =====");

        System.out.println(context.getBean(BeanA.class).hashCode());
        System.out.println(context.getBean(BeanB.class).hashCode());
        System.out.println(context.getBean(BeanC.class).hashCode());

        System.out.println(context.getBean(OtherBeanB.class).hashCode());
        System.out.println(context.getBean(OtherBeanB.class).hashCode());
        System.out.println(context.getBean(OtherBeanC.class).hashCode());
        System.out.println(context.getBean(OtherBeanC.class).hashCode());

        context.getBean(FlowerOrder.class).printFlowers();

        System.out.println("===== Config Two =====");

        System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "test2");
        AnnotationConfigApplicationContext context1 = new AnnotationConfigApplicationContext(MyConfigTwo.class);

        System.out.println(context1.getBean(BeanD.class));
        System.out.println(context1.getBean(NarcissusFlower.class));
        System.out.println(context1.getBean(RoseFlower.class));
    }
}
