package com.epam.task2.bean.beans2;

import com.epam.task2.bean.flower.Flower;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
//@Order(Ordered.LOWEST_PRECEDENCE)
@Qualifier("rose")
public class RoseFlower implements Flower {
    @Override
    public String getFlower() {
        return "Rose";
    }
}
