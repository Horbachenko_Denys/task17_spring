package com.epam.task2.bean.beans2;

import com.epam.task2.bean.flower.Flower;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
//@Order(1)
@Primary
public class NarcissusFlower implements Flower {
    @Override
    public String getFlower() {
        return "Narcissus";
    }
}
