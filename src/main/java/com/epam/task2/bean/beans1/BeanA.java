package com.epam.task2.bean.beans1;

import com.epam.task2.bean.other.OtherBeanA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeanA {
    private OtherBeanA otherBeanA;

    @Autowired
    public BeanA(OtherBeanA otherBeanA) {
        this.otherBeanA = otherBeanA;
    }
}
