package com.epam.task2.bean.beans1;

import com.epam.task2.bean.other.OtherBeanC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class BeanC {
    @Qualifier("otherBeanC")
    private OtherBeanC otherBeanC;
}
