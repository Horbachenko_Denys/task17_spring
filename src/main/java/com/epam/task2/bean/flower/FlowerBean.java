package com.epam.task2.bean.flower;

import com.epam.task2.bean.beans2.NarcissusFlower;
import com.epam.task2.bean.beans2.RoseFlower;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class FlowerBean {
    @Autowired
    @Qualifier("narcissus")
    private NarcissusFlower narcissusFlower;

    @Autowired
    @Qualifier("rose")
    private RoseFlower roseFlower;
}
