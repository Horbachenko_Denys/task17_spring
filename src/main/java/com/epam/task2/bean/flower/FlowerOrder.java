package com.epam.task2.bean.flower;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FlowerOrder {
    @Autowired
    private List<Flower> flowerList;

    public void printFlowers() {
        for (Flower flower : flowerList) {
            System.out.println(flower.getFlower());
        }
    }
}
