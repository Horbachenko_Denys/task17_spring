package com.epam.task2.bean.other;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanB {

    public String name = "OtherBeanB";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
