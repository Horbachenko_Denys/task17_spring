package com.epam.task2.bean.other;

import org.springframework.stereotype.Component;

@Component
public class OtherBeanA {

    public String name = "OtherBeanA";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
