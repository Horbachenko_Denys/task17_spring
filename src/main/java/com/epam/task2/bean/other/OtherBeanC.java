package com.epam.task2.bean.other;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("otherBeanC")
public class OtherBeanC {

    public String name = "OtherBeanC";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
