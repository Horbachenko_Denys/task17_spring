package com.epam.task2.config;

import com.epam.task2.bean.beans1.BeanA;
import com.epam.task2.bean.beans1.BeanB;
import com.epam.task2.bean.beans1.BeanC;
import com.epam.task2.bean.other.OtherBeanA;
import com.epam.task2.bean.other.OtherBeanB;
import com.epam.task2.bean.other.OtherBeanC;
import org.springframework.context.annotation.*;


@Configuration
@Profile("test")
@ComponentScan("com.epam.task2.bean.beans1")
@Import(MyFlowerConfig.class)
public class MyConfigOne {

    @Bean
    public BeanA beanA() {
        return new BeanA(otherBeanA());
    }

    @Bean
    public BeanB beanB() {
        BeanB beanB = new BeanB();
        beanB.setOtherBeanB(otherBeanB());
        return beanB;
    }

    @Bean
    public BeanC beanC() {
        return new BeanC();
    }

    @Bean
    @Scope("singleton")
    public OtherBeanA otherBeanA(){
        return new OtherBeanA();
    }

    @Bean
    @Scope("prototype")
    public OtherBeanB otherBeanB(){
        return new OtherBeanB();
    }

    @Bean
    @Scope("prototype")
    public OtherBeanC otherBeanC(){
        return new OtherBeanC();
    }
}
