package com.epam.task2.config;

import com.epam.task2.bean.flower.FlowerOrder;
import com.epam.task2.bean.beans2.NarcissusFlower;
import com.epam.task2.bean.beans2.RoseFlower;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class MyFlowerConfig {

    @Bean
    public FlowerOrder flowerBean() {
        return new FlowerOrder();
    }

    @Bean
    public NarcissusFlower narcissusFlower() {
        return new NarcissusFlower();
    }

    @Bean
    public RoseFlower roseFlower() {
        return new RoseFlower();
    }
}
