package com.epam.task1.bean;

import com.epam.task1.validator.MyBeanValidator;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof MyBeanValidator) {
            if (((MyBeanValidator) bean).validate()) {
                System.out.println(beanName + " is valid!");
            }
        }
        return bean;
    }
}
