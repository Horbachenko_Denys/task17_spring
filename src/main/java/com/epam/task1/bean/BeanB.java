package com.epam.task1.bean;


import com.epam.task1.validator.MyBeanValidator;
import org.springframework.beans.factory.annotation.Value;

public class BeanB implements MyBeanValidator {
    @Value("${beanB.name}")
    private String name;
    @Value("${beanB.value}")
    private int value;

    private void init() {
        System.out.println("BeanB init()");
    }

    private void destroy() {
        System.out.println("BeanB destroy()");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Bean name: " + this.getName() + ", bean value: " + this.getValue();
    }

    @Override
    public boolean validate() {
        return !name.isEmpty() && value >= 0;
    }
}
