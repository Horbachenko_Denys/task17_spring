package com.epam.task1.bean;


import com.epam.task1.validator.MyBeanValidator;

public class BeanF implements MyBeanValidator {
    private String name;
    private int value;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Bean name: " + this.getName() + ", bean value: " + this.getValue();
    }

    @Override
    public boolean validate() {
        return !name.isEmpty() && value >= 0;
    }
}
