package com.epam.task1.bean;


import com.epam.task1.validator.MyBeanValidator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements MyBeanValidator {
    private String name;
    private int value;

    public BeanE(BeanA beanA) {
        this.name = beanA.getName();
        this.value = beanA.getValue();
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("BeanE: postConstruct()");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("BeanE: preDestroy()");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Bean name: " + this.getName() + ", bean value: " + this.getValue();
    }

    @Override
    public boolean validate() {
        return !name.isEmpty() && value >= 0;
    }
}
