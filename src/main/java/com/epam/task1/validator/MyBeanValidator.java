package com.epam.task1.validator;

public interface MyBeanValidator {

    boolean validate();
}
