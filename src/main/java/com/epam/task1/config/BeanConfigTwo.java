package com.epam.task1.config;

import com.epam.task1.bean.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

import javax.annotation.PreDestroy;

@Configuration
@PropertySource("classpath:main.properties")
public class BeanConfigTwo {

    @Bean(value = "beanB", initMethod = "init", destroyMethod = "destroy")
    @DependsOn("beanD")
    public BeanB getBeanB() {
        return new BeanB();
    }

    @Bean(value = "beanC", initMethod = "init", destroyMethod = "destroy")
    @DependsOn("beanB")
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(value = "beanD", initMethod = "init", destroyMethod = "destroy")
    public BeanD getBeanD() {
        return new BeanD();
    }

    @Bean(name = "beanA1")
    @Qualifier("beanA1")
    public BeanA getBeanA(BeanB getBeanB, BeanC getBeanC) {
        return new BeanA(getBeanB, getBeanC);
    }

    @Bean(name = "beanA2")
    @Qualifier("beanA2")
    public BeanA getBeanA2(BeanB getBeanB, BeanD getBeanD) {
        return new BeanA(getBeanB, getBeanD);
    }

    @Bean(name = "beanA3")
    @Qualifier("beanA3")
    public BeanA getBeanA3(BeanC getBeanC, BeanD getBeanD) {
        return new BeanA(getBeanC, getBeanD);
    }

    @PreDestroy
    @Bean(name = "beanE1")
    public BeanE getBeanE1(@Qualifier("beanA1")BeanA getBeanA) {
        return new BeanE(getBeanA);
    }

    @Bean(name = "beanE2")
    public BeanE getBeanE2(@Qualifier("beanA2")BeanA getBeanA) {
        return new BeanE(getBeanA);
    }

    @Bean(name = "beanE3")
    public BeanE getBeanE3(@Qualifier("beanA3")BeanA getBeanA) {
        return new BeanE(getBeanA);
    }

    @Bean(name = "beanF")
    @Lazy
    public BeanF getBeanF() {
        return new BeanF();
    }

    @Bean
    public MyBeanPostProcessor beanPostProcessor() {
        return new MyBeanPostProcessor();
    }

    @Bean
    public MyBeanFactoryPostProcessor factoryPostProcessor() {
        return new MyBeanFactoryPostProcessor();
    }

}
