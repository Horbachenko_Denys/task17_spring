package com.epam.task1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import com.epam.task1.bean.MyBeanFactoryPostProcessor;

@Configuration
@Import(BeanConfigTwo.class)
public class BeanConfigOne {
}
